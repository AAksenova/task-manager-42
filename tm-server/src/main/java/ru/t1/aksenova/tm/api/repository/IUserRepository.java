package ru.t1.aksenova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user " +
            "(id,login,password_hash,email,first_name,last_name,middle_name,role,locked) " +
            "VALUES (#{user.id}, #{user.login}, #{user.passwordHash}, #{user.email}, #{user.firstName}, " +
            "#{user.lastName}, #{user.middleName}, #{user.role}, #{user.locked})")
    void add(@NotNull @Param("user") UserDTO user);

    @Delete("DELETE FROM tm_user where id = #{user.id}")
    void removeOne(@NotNull @Param("user") UserDTO user);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable List<UserDTO> findAll();

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByEmail(@NotNull @Param("email") String email);

    @Select("SELECT COUNT(1) FROM tm_user")
    int getSize();

    @Update("UPDATE tm_user SET login = #{user.login}, password_hash = #{user.passwordHash}, email = #{user.email}, " +
            "first_name = #{user.firstName}, last_name = #{user.lastName}, middle_name = #{user.middleName}, " +
            "role = #{user.role}, locked = #{user.locked} " +
            "WHERE id = #{user.id} ")
    void update(@NotNull @Param("user") UserDTO user);

}
