package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.ITaskService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.comparator.StatusComparator;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO add(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.add(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO add(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.addWithUserId(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks) {
        if (tasks.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            tasks.forEach(repository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<TaskDTO> comparator
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull List<TaskDTO> tasks = Collections.emptyList();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            if (StatusComparator.INSTANCE.equals(comparator)) {
                tasks = repository.findAllOrderByStatus(userId);
            } else if (NameComparator.INSTANCE.equals(comparator)) {
                tasks = repository.findAllOrderByName(userId);
            } else {
                tasks = repository.findAllOrderByCreated(userId);
            }
            return tasks;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull List<TaskDTO> tasks;
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            tasks = repository.findAllByProjectId(userId, projectId);
        }
        return tasks;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            @Nullable final TaskDTO task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            @Nullable final TaskDTO task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.removeAll(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO remove(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.removeOne(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        remove(userId, task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String project_id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(project_id);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.updateTaskProjectId(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return (repository.findOneById(userId, id) != null);
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.getSize(userId);
        }
    }

}
