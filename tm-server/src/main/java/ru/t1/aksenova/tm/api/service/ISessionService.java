package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.SessionDTO;

import java.util.Date;
import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@Nullable SessionDTO session);

    @NotNull
    SessionDTO create(@Nullable String userId, @Nullable String role);

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    SessionDTO remove(@Nullable SessionDTO session);

    @Nullable
    SessionDTO removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    SessionDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String role,
            @Nullable Date date
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);
}
