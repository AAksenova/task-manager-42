package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    @SneakyThrows
    UserDTO add(@Nullable UserDTO user);

    @NotNull
    @SneakyThrows
    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @NotNull
    UserDTO findByLogin(@Nullable String login);

    @NotNull
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOneByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOneById(@Nullable String id);

    void clear();

    @Nullable
    UserDTO remove(@Nullable UserDTO user);

    @Nullable
    UserDTO removeOneByLogin(@Nullable String login);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
