package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IDatabaseProperty;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = factory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getConnection() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @SneakyThrows
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String user = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseConnection();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("tm", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        // configuration.setCacheEnabled(true);
        // configuration.addCache(new LoggingHazelcastCache("hz"));
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(URL, databaseProperty.getDatabaseConnection());
        settings.put(USER, databaseProperty.getDatabaseUser());
        settings.put(PASS, databaseProperty.getDatabasePassword());
        settings.put(DIALECT, databaseProperty.getDatabaseHibernateDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHibernateHbm2ddl());
        settings.put(SHOW_SQL, databaseProperty.getDatabaseHibernateShowSql());
        settings.put(FORMAT_SQL, databaseProperty.getDatabaseHibernateFormatSql());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
