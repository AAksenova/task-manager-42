package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO model);

    @NotNull
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model);

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator<ProjectDTO> comparator);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    void clear();

    @SneakyThrows
    void removeAll(@Nullable String userId);

    @Nullable
    ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO project);

    @Nullable
    ProjectDTO removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);
}
