package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.entity.UserNotFoundException;
import ru.t1.aksenova.tm.exception.field.EmailEmptyException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.LoginEmptyException;
import ru.t1.aksenova.tm.exception.field.PasswordEmptyException;
import ru.t1.aksenova.tm.exception.user.ExistEmailException;
import ru.t1.aksenova.tm.exception.user.ExistLoginException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;
import ru.t1.aksenova.tm.util.HashUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService,
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService,
            @NotNull final ISessionService sessionService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.propertyService = propertyService;
        this.connectionService = connectionService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO add(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Collection<UserDTO> set(@NotNull Collection<UserDTO> users) {
        if (users.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            users.forEach(repository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return users;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistEmailException(email);
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = repository.findOneById(id);
            if (user == null) throw new TaskNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = repository.findOneById(id);
            if (user != null) remove(user);
            return user;
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            sessionService.clear();
            taskService.clear();
            projectService.clear();
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO remove(@Nullable final UserDTO user) {
        if (user == null) throw new TaskNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            sessionService.removeAll(user.getId());
            taskService.removeAll(user.getId());
            projectService.removeAll(user.getId());
            repository.removeOne(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        return remove(user);
    }

    @Nullable
    @Override
    public UserDTO removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            UserDTO user = repository.findByLogin(login);
            return (user != null);
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            return (repository.findByEmail(email) != null);
        }
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        user.setLocked(true);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        user.setLocked(false);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
