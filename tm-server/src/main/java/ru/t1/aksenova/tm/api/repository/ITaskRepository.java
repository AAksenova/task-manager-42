package ru.t1.aksenova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.TaskDTO;

import java.sql.ResultSet;
import java.util.List;

public interface ITaskRepository {

    TaskDTO fetch(@NotNull ResultSet row);

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id) " +
            "VALUES (#{task.id}, #{task.created}, #{task.name}, #{task.description}, #{task.status}, " +
            "#{task.userId}, #{task.projectId})")
    void add(@NotNull @Param("task") TaskDTO task);

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id) " +
            "VALUES (#{task.id}, #{task.created}, #{task.name}, #{task.description}, #{task.status}, " +
            "#{userId}, #{task.projectId})")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("task") TaskDTO task);

    @Delete("DELETE FROM tm_task where user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task where user_id = #{userId} AND id = #{task.id}")
    void removeOne(@NotNull @Param("userId") String userId, @NotNull @Param("task") TaskDTO task);

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT * FROM tm_task where user_id = #{userId} and project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_task SET name = #{task.name}, description = #{task.description}, status = #{task.status} " +
            "WHERE id = #{task.id} ")
    void update(@NotNull @Param("task") TaskDTO task);

    @Update("UPDATE tm_task SET project_id = #{task.projectId} WHERE id = #{task.id}")
    void updateTaskProjectId(@NotNull @Param("task") TaskDTO task);

}
