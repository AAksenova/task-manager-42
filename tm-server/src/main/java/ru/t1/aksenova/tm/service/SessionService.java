package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.ISessionService;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.entity.SessionNotFoundException;
import ru.t1.aksenova.tm.exception.field.DateEmptyException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;

import java.util.Date;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO add(@Nullable final SessionDTO session) {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            repository.add(session);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO create(
            @Nullable final String userId,
            @Nullable final String role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        @NotNull SessionDTO session = new SessionDTO();
        session.setUserId(userId);
        session.setRole(Role.valueOf(role));
        add(session);
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.findAll(userId);
        }
    }


    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            @Nullable final SessionDTO session = repository.findOneById(userId, id);
            if (session == null) throw new SessionNotFoundException();
            return session;
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            repository.removeAll(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO remove(@Nullable final SessionDTO session) {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            repository.removeOne(session);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO session = findOneById(userId, id);
        remove(session);
        return session;
    }


    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String role,
            @Nullable final Date date
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @Nullable final SessionDTO session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        session.setRole(Role.valueOf(role));
        session.setDate(date);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            repository.update(session);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return session;
    }


    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return (repository.findOneById(userId, id) != null);
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.getSize(userId);
        }
    }


}
