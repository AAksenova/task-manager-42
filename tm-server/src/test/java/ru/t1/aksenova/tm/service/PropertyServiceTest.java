package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Test
    @Ignore
    public void getApplicationVersion() {
        Assert.assertNotNull(propertyService.getApplicationVersion());
    }

    @Test
    public void getApplicationConfig() {
        Assert.assertNotNull(propertyService.getApplicationConfig());
    }

    @Test
    @Ignore
    public void getAuthorName() {
        Assert.assertNotNull(propertyService.getAuthorName());
    }

    @Test
    public void getAuthorEmail() {
        Assert.assertNotNull(propertyService.getAuthorEmail());
    }

    @Test
    public void getGitBranch() {
        Assert.assertNotNull(propertyService.getGitBranch());
    }

    @Test
    public void getGitCommitId() {
        Assert.assertNotNull(propertyService.getGitCommitId());
    }

    @Test
    public void getGitCommitTime() {
        Assert.assertNotNull(propertyService.getGitCommitTime());
    }

    @Test
    public void getGitCommitMessage() {
        Assert.assertNotNull(propertyService.getGitCommitMessage());
    }

    @Test
    public void getGitCommitterName() {
        Assert.assertNotNull(propertyService.getGitCommitterName());
    }

    @Test
    public void getGitCommitterEmail() {
        Assert.assertNotNull(propertyService.getGitCommitterEmail());
    }

    @Test
    public void getPasswordIteration() {
        Assert.assertNotNull(propertyService.getPasswordIteration());
    }

    @Test
    public void getPasswordSecret() {
        Assert.assertNotNull(propertyService.getPasswordSecret());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

}
