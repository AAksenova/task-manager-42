package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService, taskService, projectService, sessionService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @BeforeClass
    public static void initData() {
        @NotNull final UserDTO user = userService.add(USER_TEST);
        userId = user.getId();
        @NotNull final UserDTO admin = userService.add(ADMIN_TEST);
        adminId = admin.getId();
        USER_PROJECT1.setUserId(userId);
        USER_PROJECT2.setUserId(userId);
        ADMIN_PROJECT1.setUserId(adminId);
        ADMIN_PROJECT2.setUserId(adminId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable UserDTO user = userService.findOneById(userId);
        if (user != null) userService.remove(user);
        user = userService.findOneById(adminId);
        if (user != null) userService.remove(user);
    }


    @Before
    public void before() {
        projectService.add(USER_PROJECT1);
        projectService.add(USER_PROJECT2);
    }

    @After
    public void after() {
        projectService.removeAll(userId);
        projectService.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> projectService.add(NULL_PROJECT));
        Assert.assertNotNull(projectService.add(ADMIN_PROJECT1));
        @Nullable final ProjectDTO project = projectService.findOneById(adminId, ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(ADMIN_PROJECT1.getStatus(), project.getStatus());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(projectService.set(ADMIN_PROJECT_LIST));
        for (final ProjectDTO project : ADMIN_PROJECT_LIST) {
            @Nullable final ProjectDTO project2 = projectService.findOneById(adminId, project.getId());
            Assert.assertEquals(project2.getName(), project.getName());
            Assert.assertEquals(project2.getId(), project.getId());
            Assert.assertEquals(project2.getUserId(), project.getUserId());
            Assert.assertEquals(project2.getStatus(), project.getStatus());
        }
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> projectService.add(null, ADMIN_PROJECT1));
        Assert.assertThrows(AbstractException.class, () -> projectService.add("", ADMIN_PROJECT1));
        Assert.assertThrows(AbstractException.class, () -> projectService.add(adminId, NULL_PROJECT));
        Assert.assertNotNull(projectService.add(adminId, ADMIN_PROJECT1));
        @Nullable final ProjectDTO project = projectService.findOneById(adminId, ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(ADMIN_PROJECT1.getStatus(), project.getStatus());
    }

    @Test
    public void createByUserId() {
        Assert.assertThrows(AbstractException.class, () -> projectService.create(null, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.create(adminId, null, ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.create(adminId, ADMIN_PROJECT1.getName(), null));
        @NotNull final ProjectDTO project = projectService.create(adminId, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(adminId, project.getUserId());
    }

    @Test
    public void updateByUserIdById() {
        Assert.assertThrows(AbstractException.class, () -> projectService.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateById(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateById(userId, NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateById(userId, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null));
        @NotNull final ProjectDTO project = projectService.updateById(userId, USER_PROJECT1.getId(), PROJECT_NAME, PROJECT_DESCR);
        Assert.assertEquals(PROJECT_NAME, project.getName());
        Assert.assertEquals(PROJECT_DESCR, project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    private int getIndexFromList(@NotNull final List<ProjectDTO> projects, @NotNull final String id) {
        int index = 0;
        for (ProjectDTO project : projects) {
            index++;
            if (id.equals(project.getId())) return index - 1;
        }
        return -1;
    }

    @Test
    public void updateByUserIdByIndex() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll(userId);
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> projectService.updateByIndex(null, index, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateByIndex(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateByIndex(userId, -1, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateByIndex(userId, index, null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> projectService.updateByIndex(userId, index, USER_PROJECT1.getName(), null));
        @NotNull final ProjectDTO project = projectService.updateByIndex(userId, index, PROJECT_NAME, PROJECT_DESCR);
        Assert.assertEquals(PROJECT_NAME, project.getName());
        Assert.assertEquals(PROJECT_DESCR, project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusById(null, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusById(userId, USER_PROJECT1.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusById(userId, NON_EXISTING_PROJECT_ID, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusById(NON_EXISTING_USER_ID, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        @NotNull final ProjectDTO project = projectService.changeProjectStatusById(userId, USER_PROJECT1.getId(), Status.IN_PROGRESS);
        @NotNull final ProjectDTO project2 = projectService.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertEquals(project2.getId(), project.getId());
        Assert.assertEquals(project2.getStatus(), project.getStatus());
        Assert.assertEquals(project2.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll(userId);
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusByIndex(null, index, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusByIndex(userId, null, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusByIndex(userId, index, null));
        Assert.assertThrows(AbstractException.class, () -> projectService.changeProjectStatusByIndex(userId, -1, Status.COMPLETED));
        @NotNull final ProjectDTO project = projectService.changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void set() {
        projectService.removeAll(adminId);
        Assert.assertEquals(EMPTY_PROJECT_LIST, Collections.emptyList());
        projectService.set(ADMIN_PROJECT_LIST);
        final List<ProjectDTO> projects2 = projectService.findAll(adminId);
        projects2.forEach(project -> Assert.assertEquals(adminId, project.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> projectService.findAll(""));
        Assert.assertEquals(Collections.emptyList(), projectService.findAll(NON_EXISTING_USER_ID));
        final List<ProjectDTO> projects = projectService.findAll(userId);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparator() {
        projectService.removeAll(userId);
        projectService.set(USER_PROJECT_LIST);
        projectService.set(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = projectService.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        final List<ProjectDTO> projects2 = projectService.findAll(adminId, comparator);
        projects2.forEach(project -> Assert.assertEquals(adminId, project.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(userId, null));
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll(userId);
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneByIndex(USER_PROJECT1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneByIndex(null, index));
        @Nullable final ProjectDTO project = projectService.findOneByIndex(userId, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(projectService.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> projectService.existsById(userId, null));
        Assert.assertFalse(projectService.existsById(null, USER_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void removeAll() {
        projectService.removeAll(userId);
        projectService.removeAll(adminId);
        Assert.assertEquals(0, projectService.getSize(userId));
        Assert.assertEquals(0, projectService.getSize(adminId));
        projectService.set(PROJECT_LIST);
        Assert.assertNotEquals(0, projectService.getSize(userId));
        Assert.assertNotEquals(0, projectService.getSize(adminId));
    }

    @Test
    public void removeOne() {
        @Nullable final ProjectDTO project = projectService.add(ADMIN_PROJECT1);
        Assert.assertNotNull(projectService.findOneById(adminId, ADMIN_PROJECT1.getId()));
        projectService.remove(adminId, project);
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(adminId, ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> projectService.removeOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.add(ADMIN_PROJECT2);
        Assert.assertNotNull(projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
        projectService.removeOneById(adminId, project.getId());
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> projectService.removeOneById(null, ADMIN_PROJECT2.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectService.removeOneById(adminId, null));
        Assert.assertThrows(AbstractException.class, () -> projectService.removeOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.add(ADMIN_PROJECT2);
        Assert.assertNotNull(projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
        projectService.removeOneById(adminId, project.getId());
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        projectService.add(ADMIN_PROJECT1);
        @NotNull final List<ProjectDTO> projects = projectService.findAll(adminId);
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        @Nullable final ProjectDTO project2 = projectService.removeOneByIndex(adminId, index);
        Assert.assertNotNull(project2);
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(adminId, ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        projectService.add(ADMIN_PROJECT1);
        @NotNull final List<ProjectDTO> projects = projectService.findAll(adminId);
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> projectService.removeOneByIndex(USER_PROJECT1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> projectService.removeOneByIndex(null, index));
        @Nullable final ProjectDTO project2 = projectService.removeOneByIndex(adminId, index);
        Assert.assertNotNull(project2);
        Assert.assertThrows(AbstractException.class, () -> projectService.findOneById(project2.getId(), project2.getUserId()));
    }

}
